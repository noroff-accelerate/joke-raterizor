import { useContext, useEffect, useState } from "react";
import Joke from "./Joke"
import { JokeContext } from "./State/JokeContext";

function JokeRater(){

    const [joke, setJoke] = useState({});
    const [jokeReports, setJokeReports] = useContext(JokeContext)
  
  
    useEffect(() => {
      getJoke();
    }, []);
  
    function getJoke() {
      fetch(
        "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single"
      )
        .then((response) => response.json())
        .then((result) => {
          setJoke(result);
        });
    }
  
    function upVote() {
      jokeReports.roflJokes.push(joke);
      getJoke();
      console.log(jokeReports)
    }
  
    function downVote() {
        jokeReports.mehJokes.push(joke);
      getJoke();
      console.log(jokeReports)
    }

    return(
        <div>
            <h2>Joke Rater</h2>
            <Joke jokeData={joke}/>
            <button onClick={upVote}>🤣</button>
            <button onClick={downVote}>😐</button>
        </div>
    )
}

export default JokeRater