import { useContext } from "react";
import Joke from "./Joke";
import { JokeContext } from "./State/JokeContext";

function JokeReport() {
  const [jokeReports, setJokeReports] = useContext(JokeContext);
  return (
    <>
      <h1>Joke Reports</h1>
      <Joke jokeData={jokeReports.roflJokes[0]} />
    </>
  );
}

export default JokeReport;
