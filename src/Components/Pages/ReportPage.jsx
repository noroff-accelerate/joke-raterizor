import JokeReport from "../JokeReport"
import withAuth from "../withAuth"

function ReportPage(){
    return(
        <div>
            <h1>ReportPage</h1>
            <JokeReport/>
        </div>
    )
}

export default withAuth(ReportPage)