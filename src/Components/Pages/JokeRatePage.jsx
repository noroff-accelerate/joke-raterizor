import JokeRater from "../JokeRater"
import withAuth from "../withAuth"

function JokeRatePage(){
    return(
        <div>
            <h1>JokeRatePage</h1>
            <JokeRater/>
        </div>
    )
}

export default withAuth(JokeRatePage)