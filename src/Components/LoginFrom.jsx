import { useState } from "react";
import { useNavigate } from "react-router-dom";

function LoginForm() {

    const [userName, setUsername] = useState("unknown")
    const navigate = useNavigate()

    function handleLoginBtn(){
      console.log(localStorage.getItem('userName'))
        localStorage.setItem("userName",userName)
        navigate("/joke")
    }

    function updateUsername(event){
        setUsername(event.target.value)
    }

  return (
    <div>
        <h4>{userName}</h4>
      <input type="text" onChange={updateUsername}/>
      <button onClick={handleLoginBtn}>Login</button>{" "}
    </div>
  );
}

export default LoginForm;
