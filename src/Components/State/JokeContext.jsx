import { createContext, useState } from "react";

export const JokeContext = createContext({roflJokes:[], mehJokes:[]});

function JokeProvider({ children }) {
  const [jokeReports, setJokeReports] = useState({roflJokes:[], mehJokes:[]});
  return <JokeContext.Provider value={[jokeReports, setJokeReports]}>{children}</JokeContext.Provider>;
}

export default JokeProvider;
