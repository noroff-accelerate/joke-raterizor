import logo from './logo.svg';
import './App.css';
import Joke from './Components/Joke';
import { BrowserRouter, NavLink, Route, Routes } from 'react-router-dom';
import LoginPage from './Components/Pages/LoginPage';
import JokeRatePage from './Components/Pages/JokeRatePage';
import ReportPage from './Components/Pages/ReportPage';

function App() {
  return (
    <div className="App">
      
      <BrowserRouter>
      <NavLink to="/">Login</NavLink>
      <NavLink to="/joke">Joke</NavLink>
      <NavLink to="/report">Report</NavLink>
      <Routes>
        <Route path="/" element={<LoginPage/>} />
        <Route path="/joke" element={<JokeRatePage/>} />
        <Route path="/report" element={<ReportPage/>} />
      </Routes>
      
      </BrowserRouter>
    </div>
  );
}

export default App;
